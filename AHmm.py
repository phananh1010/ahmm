__author__ = 'Anh Phan Nguyen'
#input data:
#data = [['Nguyen', 'Van', 'A'], ['Nguyen', 'Thi', 'B'], ['Nguyen', 'Hoang','C'], ['Nguyen', 'Van', 'E'], ['Le', 'F']]
#init? no input, no _T, _M, _P shape, no values? create n_hstates, specifed
#given data, convert to X: X = [[1, 2, 3], [1, 5, 6], [1, 7, 8]]
#vocabulary ['nguyen', 'van', 'a', 'thi', 'b', 'hoang', 'c'
#inverse vocabulary: {1: 'nguyen', 2: 'van', 3: 'thi', ...}
#must have a inverse_vocabulary
#create M, P, T (if not specified):
#from X-> M, inverse_vocabulary, _vocabulary

#feature1: hmm support text data set format X = [X1, X2, X3] = [[x11, x12, ...], [x21, x22, ], ...]
#feature2: automatically generate T, M, P if not specified
#feature3: train T, M, P given data set
#feature4: given new label Xt = [xt1, xt2,...], answer probability of Xt given current hmm model


#feature1: convert tokenized labelset into 2d numpy array, store inverse vocabulary
#feature2:


import numpy

class AHmm:
    def __init__(self, n_hstates=0, uinit_P=False, uinit_T=False, uinit_M=False, fixed_P=False, fixed_T=False, fixed_M=False, smoothed_M=False):
        self._nS = n_hstates

        self._vocabulary = []
        self._inverse_vocabulary = {}

        self._P = numpy.zeros(0)
        self._T = numpy.zeros((0, 0))
        self._M = numpy.zeros((0, 0))

        self._smoothed_probs = numpy.zeros(0)

        self._X = []
        self._nX = 0
        self._L_list = []

        self._train_step = 100

        self._alpha_list = []
        self._beta_list = []
        self._pX_list = []
        self._gamma_list = []
        self._xi_list = []

        self._config = {
                'uinit_P': uinit_P,
                'uinit_T': uinit_T,
                'uinit_M': uinit_M,
                'fixed_P': fixed_P,
                'fixed_T': fixed_T,
                'fixed_M': fixed_M,
                'smoothed_M': smoothed_M
        }

    def _label_data_to_numerical_data(self, label_data):
        #label_data have format:
        #data = [[label1, label2], [label3, label4, label5, ], []]
        #step 1: convert label array into internal vocabulary
        for label_item in label_data:
            for label in label_item:
                if label not in self._vocabulary:
                    self._vocabulary += [label]
                    self._inverse_vocabulary[self._vocabulary.index(label)] = label

        #step 2: convert label array into number
        for label_item in label_data:
            item = []
            for label in label_item:
                item += [self._vocabulary.index(label)]

            self._X += [item]
            self._L_list += [len(item)]

        #step 3: set nX
        self._nX = len(self._vocabulary)


    #randomly generate internal states
    def _init_params(self):#, fixed_P=False, fixed_T=False, fixed_M=False
        if self._config['uinit_P'] == False:
            self._P = numpy.random.rand(self._nS)
            self._P = self._P / numpy.sum(self._P)

        if self._config['uinit_T'] == False:
            self._T = numpy.random.rand(self._nS, self._nS)
            for i in range(self._nS):
                self._T[i] = self._T[i] / sum(self._T[i])

        if self._config['uinit_M'] == False:
            self._M = numpy.random.rand(self._nS, self._nX)
            for i in range(self._nS):
                self._M[i] = self._M[i] / sum(self._M[i])


    #private calculate alpha
    def _calculate_alpha(self):
        for i,x in enumerate(self._X):
            alpha = numpy.array(numpy.zeros((self._nS, self._L_list[i])))
            alpha[:, 0] = self._P * self._M[:, x[0]]

            for k in range(1, self._L_list[i]):
                for idx_s in range(self._nS):
                    temp = 0;
                    for idx_s0 in range(self._nS):
                        temp += alpha[idx_s0][k-1] * self._T[idx_s0][idx_s]
                    alpha[idx_s][k] = self._M.item(idx_s, x[k]) * temp

            self._alpha_list += [alpha]


    #private calculate beta
    def _calculate_beta(self):
        for i,x in enumerate(self._X):
            beta = numpy.array(numpy.zeros((self._nS, self._L_list[i])))
            beta[:, self._L_list[i]-1] = 1          #states at step L equal 1

            for k in list(reversed(range(self._L_list[i] - 1))):
                for idx_s in range(self._nS):
                    beta[idx_s][k] = 0;
                    for idx_s0 in range(self._nS):
                        beta[idx_s][k] += self._T[idx_s][idx_s0] * self._M[idx_s0][x[k+1]] * beta[idx_s0][k+1]

            self._beta_list += [beta]


    #calculate sample probability
    def _normalize(self):
        for i, x in enumerate(self._X):
            self._pX_list += [sum(self._alpha_list[i].transpose()[-1])]


    #calculate gamma
    def _calculate_gamma(self):
        for idx,_ in enumerate(self._X):
            gamma_states = []
            for i in range(0, self._L_list[idx]):
                temp = self._alpha_list[idx][:, i] * self._beta_list[idx][:, i]
                gamma_states += [temp / self._pX_list[idx]]
            self._gamma_list += [numpy.vstack(gamma_states).transpose()]


    #calculate xi
    def _calculate_xi(self):
        for i,x in enumerate(self._X):
            xi = numpy.array(numpy.zeros([self._nS, self._nS, self._L_list[i]-1]))           #xi[s,q,k] if P(S_k=s,S_(k+1)=q|X_1..X_L)
            #P_X = sum(alpha.transpose()[-1])                     #probability of whole observation sequence
            #example at step k=10 S_k = s, S_(k+1) = q, then:
            #xi[s,q,k] = xi[s, q, 10] = alpha[0, 10] * T [0, 1] * M[1, X[11]-1] * beta[1,11]
            for s in range(self._nS):
                for q in range(self._nS):
                    for k in range(self._L_list[i]-1):
                        xi[s,q,k] = self._alpha_list[i][s, k] * self._T[s, q] * self._M[q, x[k+1]] * self._beta_list[i][q, k+1] / self._pX_list[i]
            self._xi_list += [xi]


    #----------------------EM algorithm section-------------------
    def _estimate_P_vector(self):
        P_prime = numpy.array(numpy.zeros(self._nS))
        normalizer = 0
        for g in self._gamma_list:
            normalizer += sum(g[:,0])   #all states prior prob at step 0, should be sum to 1

        for s in range(self._nS):
            P_prime[s] = numpy.sum([self._gamma_list[i][s,0] for i in range(len(self._L_list))])/normalizer

        self._P = P_prime
        return P_prime


    def _estimate_T_matrix(self):
        T_prime = numpy.array(numpy.zeros([self._nS, self._nS]))

        for s in range(self._nS):
            normalizer = 0              #normalize xi[s,i] for i in range(nS)
            for t in range(self._nS):
                T_prime[s,t] = 0
                for i,_ in enumerate(self._X):
                    T_prime[s,t] += numpy.sum(self._xi_list[i][s,t])
                normalizer += T_prime[s,t]
            T_prime[s] = T_prime[s,:]/normalizer

        self._T = T_prime
        return T_prime


    #smoothing the emission distribution, not to be confused with smoothing technique (gamma)
    def _smooth_dist(self):
        #the EM algorithm calculate P(s_i|s_j) = E[#s_i->obs]/E[#s_i]
        #before smoothing, first, calculate E[#s_i]

        if self._config['smoothed_M'] == False:
            return

        #record the smoothed prob for label not in sample distribution
        smoothed_probs = numpy.array(numpy.zeros(self._nS))

        #estimate number of S_i would be in samples
        E_s_i = numpy.array(numpy.zeros(self._nS))
        for s_i in range(self._nS):
            E_s_i[s_i] = sum([sum(item[s_i,:]) for item in self._gamma_list])

        V = len(2 * self._vocabulary) - 1       #this is a very simple heuristic, assuming real vocabulary size is two time traiining vocabulary size
        M_prime = numpy.array(numpy.zeros((self._nS, len(self._vocabulary))))

        for s_i in range(self._nS):
            N = E_s_i[s_i]
            smoothed_probs[s_i] = 1/((N + V) * 1.0)
            for x_i in range(len(self._vocabulary)):
                M_prime[s_i, x_i] = (self._M[s_i, x_i] * N + 1) / (N + V)

        self._smoothed_probs = smoothed_probs
        self._M = M_prime
        return


    def _estimate_M_matrix(self):
        M_prime = numpy.array(numpy.zeros([self._nS, len(self._vocabulary)]))
        for s in range(self._nS):
            normalizer = 0      #normalizer for given s on all possible label in vocabulary
            for g in self._gamma_list:
                normalizer += numpy.sum(g[s,:])

            #find sum of gamma[s,l] for each label in vocabulary
            for observation in self._inverse_vocabulary.keys():
                M_prime[s, observation] = 0
                for i,sample in enumerate(self._X):
                    for j,label in enumerate(sample):
                        if label == observation:
                            M_prime[s, observation] += self._gamma_list[i][s,j]
                M_prime[s, observation] = M_prime[s, observation] / normalizer

        self._M = M_prime

        #will do nothing if 'smoothed_M' in config is set to False
        self._smooth_dist()

        return M_prime
    #----------------------end of EM algorithm section------------

    def predict(self, observations):
        M_prime = self._M
        X = []
        for obs in observations:
            #if the test sample has observation not in hmm vocabulary itself, then:
            #1) continue to process is smoothed
            try:
                X += [self._vocabulary.index(obs)]
            except:
                if self._config['smoothed_M'] == True:
                    X += [-1]
                else:
                    raise



        #X = [self._vocabulary.index(obs) for obs in observations]
        L = len(X)
        alpha = numpy.array(numpy.zeros((self._nS, L)))

        #first column
        if X[0] != -1:
            alpha[:, 0] = self._P * self._M[:, X[0]]
        else:
            alpha[:, 0] = self._P * self._smoothed_probs

        for k in range(1, L):
            for idx_s in range(self._nS):
                temp = 0;
                for idx_s0 in range(self._nS):
                    temp += alpha[idx_s0][k-1] * self._T[idx_s0][idx_s]
                if X[k] != -1:
                    alpha[idx_s][k] = self._M.item(idx_s, X[k]) * temp
                else:
                    alpha[idx_s][k] = self._smoothed_probs[idx_s] * temp

        return sum(alpha[:,-1])


    #def _generate_hmm_params(self, ):
    def fit(self, label_data):
        self._label_data_to_numerical_data(label_data)
        self._init_params()
        self._calculate_alpha()
        self._calculate_beta()
        self._normalize()
        self._calculate_gamma()
        self._calculate_xi()

        for i in range(self._train_step):
            if self._config['fixed_P'] == False:
                self._estimate_P_vector()
            if self._config['fixed_T'] == False:
                self._estimate_T_matrix()
            if self._config['fixed_M'] == False:
                self._estimate_M_matrix()

            self._alpha_list = []
            self._beta_list = []
            self._pX_list = []
            self._gamma_list = []
            self._xi_list = []

            self._calculate_alpha()
            self._calculate_beta()
            self._normalize()
            self._calculate_gamma()
            self._calculate_xi()



def test_1():
    #test
    data = [['Nguyen', 'Van', 'A'], ['Nguyen', 'Thi', 'B'], ['Nguyen', 'Hoang', 'C'], ['Nguyen', 'Van', 'E'], ['Le', 'F']]
    ahmm = AHmm()
    del ahmm
    ahmm = AHmm(3)
    ahmm.fit(data)
    M = ahmm._estimate_M_matrix()
    print (M)
    return

def test_2():
    data = [[3, 1, 1, 3, 2, 3, 2, 2, 2, 3, 2, 2, 2, 2, 2, 3, 3, 1, 1, 2]]
    ahmm = AHmm(2)
    del ahmm
    ahmm = AHmm(2, uinit_P=True,  uinit_T=True, uinit_M=True)
    ahmm._P = numpy.array([.5, .5])
    ahmm._T = numpy.array([[.5, .5], [.5, .5]])
    ahmm._M = numpy.array([[.4, .1, .5], [.1, .5, .4]])
    ahmm.fit(data)
    return ahmm


def test_3():
    ahmm = AHmm()
    del ahmm
    ahmm = AHmm(3, uinit_M=True, uinit_T=True, uinit_P=True)
    ahmm._train_step = 0
    ahmm._P = numpy.array([1, 0, 0])
    ahmm._T = numpy.array([[.1, .4, .5], [.4, 0, .6], [0, .6, .4]])
    ahmm._M = numpy.array([[.6, .2, .2], [.2, .6, .2], [.2, .2, .6]])
    data = [[1, 3, 3, 2]]
    ahmm.fit(data)
    return ahmm



